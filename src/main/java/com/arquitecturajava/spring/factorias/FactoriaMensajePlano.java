package com.arquitecturajava.spring.factorias;

import com.arquitecturajava.spring.Mensaje;
import com.arquitecturajava.spring.MensajePlano;

public class FactoriaMensajePlano implements FactoriaMensajes {

	public Mensaje getMensaje() {
		return new MensajePlano();
	}
}
