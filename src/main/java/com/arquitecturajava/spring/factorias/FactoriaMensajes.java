package com.arquitecturajava.spring.factorias;

import com.arquitecturajava.spring.Mensaje;
import com.arquitecturajava.spring.MensajeHTML;
import com.arquitecturajava.spring.MensajePlano;

public  interface FactoriaMensajes {
		
	public Mensaje getMensaje();
}
