package com.arquitecturajava.spring.factorias;

import com.arquitecturajava.spring.Mensaje;
import com.arquitecturajava.spring.MensajeHTML;

public class FactoriaMensajeHTML implements FactoriaMensajes{

	public Mensaje getMensaje() {
		return new MensajeHTML();
	}
}
