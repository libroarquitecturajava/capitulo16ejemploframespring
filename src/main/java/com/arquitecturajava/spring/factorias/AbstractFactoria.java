package com.arquitecturajava.spring.factorias;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


import com.arquitecturajava.spring.Mensaje;
import com.arquitecturajava.spring.Principal;

public abstract class AbstractFactoria {
	
	
	public static FactoriaMensajes getInstance()  {
		
		Properties  propiedades = new Properties();
		String tipoMensaje="";
		try {
			propiedades.load(Principal.class.getResourceAsStream(
					"mensaje.properties"));
			tipoMensaje = propiedades.getProperty("tipo");
		}catch(Exception e ) {
			System.out.println(e.getMessage());
		}
		 
		if(tipoMensaje.equalsIgnoreCase("HTML")) {
			return new FactoriaMensajeHTML();
		}else {
			return new FactoriaMensajePlano();
		}
	}
}
