package com.arquitecturajava.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.arquitecturajava.spring.factorias.AbstractFactoria;
import com.arquitecturajava.spring.factorias.FactoriaMensajes;

public class Principal {

	
	public static void main(String[] args) {
		
		//FactoriaMensajes mensajeria = AbstractFactoria.getInstance();
		//mensajeria.getMensaje().hola();
		
		ApplicationContext factoria = new 
				FileSystemXmlApplicationContext("src/main/resources/contextoAplicacion.xml");
		Mensaje miMensaje = (Mensaje) factoria.getBean("mensajeHTML");
		miMensaje.hola();
		Mensaje plano = (Mensaje) factoria.getBean("mensajeBasico");
		plano.hola();
	}
}
