# capitulo16EjemploFrameSpring

# Este proyecto es un ejemploo de como instalar spring en una aplicacion
# Cabe mencionar que los jar no se pudieron descargar con maven por lo que
# se utilizo las siguientes repos para desargar los jars los demas jars se
# descargaron de maven y no son para spring
https://repo1.maven.org/maven2/org/springframework/spring-asm/3.0.5.RELEASE/
https://repo1.maven.org/maven2/org/springframework/spring-beans/3.0.5.RELEASE/
https://repo1.maven.org/maven2/org/springframework/spring-context/3.0.5.RELEASE/
https://repo1.maven.org/maven2/org/springframework/spring-expression/3.0.5.RELEASE/
https://repo1.maven.org/maven2/org/springframework/spring-core/3.0.5.RELEASE/
